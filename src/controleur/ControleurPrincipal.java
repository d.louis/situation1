package controleur;
import DAO.ConnexionPostgreSql;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import classe.Jeu;
import classe.Joueur;
import DAO.JeuDAO;
import DAO.DAO;
import DAO.JoueurDAO;
import vues.Vue_principale;

public class ControleurPrincipal implements ActionListener{

	// définition de l’objet instance de MaVue (vue principale)
	private Vue_principale vue;
	// déclaration des objets Modele qui permettront d’obtenir ou de transmettre les données :
	private DAO<Jeu> gestionJeu;
	private JoueurDAO gestionJoueur;
	// déclarations des éventuelles propriétés utiles au contrôleur
	public static List<Jeu> lesJeu = new ArrayList<Jeu>();
	public static List<Joueur> lesJoueurs = new ArrayList<Joueur>();

	
	// Constructeur
	public ControleurPrincipal()
	{
		this.gestionJeu=new JeuDAO();
		this.gestionJoueur= new JoueurDAO();
		// + initialisations éventuelles autres propriétés définies dans cette classe
		this.lesJeu=gestionJeu.recupAll();
	}

	/*
	@Override
	public void actionPerformed(ActionEvent e) // Méthode qu'il faut implémenter
	{
		// quand on clique sur Quitter
		if (e.getActionCommand().equals("Quitter"))
				{
				Quitter();
				// déconnection bdd et arrêt de l’application
				}
		
		else if (e.getActionCommand().equals("comboBox")) {
			if (vue.comboBoxListe.getSelectedIndex()!= 0) {
				vue.affichageInfoClub(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1));
				//gestionLicencie.recupLicencie(lesLicencies.get(vue.comboBoxListe.getSelectedIndex()-1).getCode());
				 
				vue.afficher_licencie(gestionLicencie.recupLicencie(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1).getCode()));
				 
			}
			else {
				vue.lblAfficheInfoClub.setText("");
			}
		}
		// si on clique sur le bouton enregistrer (un nouveau licencié)
		else if (e.getActionCommand().equals("Ajouter"))
				{
					vue.ajouter();
					
				// insertion d’un nouveau licencié dans la bdd en récupérant les données de la vue
				// retour à la vue avec mise à jour des licenciés du club dans la zone prévue
				}
		// si un autre évènement est à l’origine de l’appel au contrôleur
		else if (e.getActionCommand().equals("Supprimer"))
				{
				//... / ...
				}
		

	}
	
	public void Quitter() {
		ConnexionPostgreSql.Arreter(); 
		System.exit(0);
		
	}

*/
	public void setVues(Vue_principale view) {
		this.vue=view;
		vue.remplirComboJeu(lesJeu);
	
	
	}
	public void Quitter() {
		ConnexionPostgreSql.Arreter(); 
		System.exit(0);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) // Méthode qu'il faut implémenter
	{
		// quand on clique sur Quitter
		if (e.getActionCommand().equals("Quitter"))
				{
				Quitter();
				// déconnection bdd et arrêt de l’application
				}
		
		else if (e.getActionCommand().equals("comboBox")) {
			if (vue.affiche_jeu.getSelectedIndex()!= 0) {
				vue.affichageInfoJeu(lesJeu.get(vue.affiche_jeu.getSelectedIndex()-1));				 
			//	vue.affiche_jeu(gestion.recupJeu(lesJeu.get(vue.affiche_jeu.getSelectedIndex()-1).getId()));
				 
			}
	
		}
		
}
}