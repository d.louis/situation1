package classe;
 
import java.util.List;
import java.util.Random;


public class Jeu {
	private int id;
	private String nom;
	private String description;
	private String date_creation;
	private int prix;
	private String image;
	private String config_recommandee;
	private List<Joueur> joueurs_jeu;
	 /**
    *
    * constructeur standard de jeu
    */
	public Jeu() {
		id=0;
		nom= null;
		description =null;
		date_creation = null;
		prix = 0;
		image =null;
		config_recommandee = null;
	}
	 /**
    *
    * Permet de construire un objet jeu
    */
	public Jeu(int id,String nom, String description, String date_creation,
	 int prix, String image, String config_recommandee,List<Joueur> joueurs_jeu) {
		this.id=id;
		this.nom= nom;
		this.description =description;
		this.date_creation = date_creation;
		this.prix = prix;
		this.image = image;
		this.config_recommandee = config_recommandee;
		this.joueurs_jeu =joueurs_jeu;
	}
	public Jeu(int id,String nom, String description, String date_creation,
			 int prix, String image, String config_recommandee) {
				this.id=id;
				this.nom= nom;
				this.description =description;
				this.date_creation = date_creation;
				this.prix = prix;
				this.image = image;
				this.config_recommandee = config_recommandee;
				
			}
	 public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/**
    *
    * Permet d'obtenir le nom du jeu
    * @return - le nom du jeu sous forme de chaine de caract�res
    */
	public String getNom()  {
	   	 return nom;
	    }
	 /**
    *
    * Permet de changer le nom du jeu
    */
	 public void setnom(String nom) {
	   	 this.nom = nom;
	    }
	
	 /**
     * Permet d'obtenir la description du jeu
     * @return - la description du jeu sous forme de chaine de caract�res
     */
	 public String getDescription()  {
	   	 return description;
	    }
	 /**
	    *
	    * Permet de changer la description du jeu
	    */
	 public void setDescription(String description) {
	   	 this.description = description;
	    }
	 /**
     *
     * Permet d'obtenir la description du jeu
     * @return - la date de creation du jeu sous forme de chaine de caract�res
     */
	 public String getDate_creation()  {
 	 return date_creation;
	 }
	 /**
	    *
	    * Permet de changer la date de creation du jeu
	    */
	 public void setDate_creation(String date_creation) {
 	 this.date_creation = date_creation;
	 }
	 /**
     *
     * Permet d'obtenir le prix du jeu
     * @return - le prix du jeu sous forme d'entier
     */
		public int getPrix()  {
		   	 return prix;
		    }
		 /**
	    *
	    * Permet de changer le prix d'un jeu
	    */
		 public void setPrix(int prix) {
		   	 this.prix = prix;
		    }
		 /**
	     * Permet d'obtenir la description du jeu
	     * @return - l'image du jeu sous forme de chaine de caract�res
	     */
		 public String getImage()  {
		   	 return image;
		    }
		 /**
		    *
		    * Permet de changer l'image d'un jeu
		    */
		 public void setImage(String image) {
		   	 this.image = image;
		    }
		 /**
	     *
	     * Permet d'obtenir la description du jeu
	     * @return - la config_recommandee du jeu sous forme de chaine de caract�res
	     */
	public String getConfig_recommandee()  {
	 	 return config_recommandee;
	  }
	 /**
    * Permet de changer la configuration recommand�e d'un jeu
    */
	public void setConfig_recommandee(String config_recommandee) {
	 	 this.config_recommandee = config_recommandee;
	  }
	 /**
	    * Permet de g�n�rer une date al�atoire pour un match
	    */
	public String genere_date_prochain_match() {
        Random aleatoire = new Random();
        int annee = aleatoire.nextInt(20);
        int mois = aleatoire.nextInt(12);
        int jour = aleatoire.nextInt(31);
        String date = (jour +  " / " + mois + " /20" + annee);
        return date;
	}
	
	public List<Joueur> getJoueurs_jeu() {
		return joueurs_jeu;
	}
	public void setJoueurs_jeu(List<Joueur> joueurs_jeu) {
		this.joueurs_jeu = joueurs_jeu;
	}
	
}
