package DAO;
 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import classe.Jeu;
import classe.Joueur;

public class JoueurDAO extends DAO<Joueur> {

	
	public List<Joueur> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Joueur> listeJoueur = new ArrayList<Joueur>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"appli_jeu\".joueurs");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Joueur unJoueur = new Joueur();
				
				unJoueur.setId(curseur.getInt("id"));
				unJoueur.setPseudo(curseur.getString("pseudo"));
				unJoueur.setnationalite(curseur.getString("nationalite"));
				unJoueur.setage(curseur.getInt("age"));
				
				listeJoueur.add(unJoueur);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeJoueur; 
	}
	@Override
	public void create(Joueur obj) {
		try {
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"appli_jeu\".joueurs VALUES(?, ?, ?, ?)"
                                         );
		 	prepare.setInt(1, obj.getId());
			prepare.setString(2, obj.getPseudo());
			prepare.setString(3, obj.getnationalite());
			prepare.setInt(4, obj.getage());
				
			prepare.executeUpdate();  
				
		}
	catch (SQLException e) {
	e.printStackTrace();
	} 
		
	}
	
	@Override
	public Joueur read(String id) {
		Joueur leJoueur = new Joueur();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"appli_jeu\".joueur WHERE id = '" + id +"'");
	          
	          if(result.first())
	           		leJoueur = new Joueur(result.getInt("id"), result.getString("pseudo"),result.getString("nationalite"),result.getInt("age"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leJoueur;
		
	}
	@Override
	public void update(Joueur obj) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void delete(Joueur obj) {
		
	}

	
}
