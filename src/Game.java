

	import java.awt.BorderLayout;
	import java.awt.EventQueue;
	import java.util.ArrayList;
	import java.util.List;

	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import javax.swing.JTextArea;
	import javax.swing.border.EmptyBorder;

import classe.Jeu;
import classe.Joueur;

import javax.swing.ImageIcon;
	import javax.swing.JComboBox;
	import javax.swing.JTextField;
	import javax.swing.JList;
	import javax.swing.JLabel;
	import java.awt.event.ActionListener;
	import java.awt.event.ActionEvent;
	import java.awt.Color;
	import java.awt.SystemColor;

	public class Game extends JFrame {

	    private JPanel contentPane;
	    private static List<Jeu> les_jeux = new ArrayList<Jeu>();
	    private JTextField textField;
	    
	    private JLabel desc;
	    private JTextArea joueur;
	    private JLabel prix ;
	    private JLabel date_creation;
	    private JLabel conf;
	    private JLabel lblNewLabel_2;
	    private JLabel date_match;
	    private JLabel image;
	    /**
	     * Launch the application.
	     */
	    public static void main(String[] args) {
	        EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                try {
	//cr�ation des jeux dans la liste
	                    
	                    les_jeux.add(new Jeu(1,"mario bros","jeu de plombier","2016/11/08" ,23,"supermariorunta.jpg","bon pc",new ArrayList<Joueur>()));
	                    les_jeux.add(new Jeu(1,"rainbow 6","jeu de tir","2016/11/08" ,28,"rainbow.jpg","bon pc",new ArrayList<Joueur>()));
						les_jeux.add(new Jeu(1,"zelda","jeu d'aventure","2017/03/03" ,35,"zelda.jpg","nintendo switch",new ArrayList<Joueur>()));
						les_jeux.add(new Jeu(1,"total war warhammer II","jeu de strat�gie","2012/08/14" ,40,"totalwar.jpg","bon pc",new ArrayList<Joueur>()));
						les_jeux.add(new Jeu(1,"total war warhammer III","jeu de strat�gie","2021/08/01" ,40,"totalwarIII.jpg","bon pc",new ArrayList<Joueur>()));
						les_jeux.add(new Jeu(1,"adibou","jeu pour enfant","2001/09/28" ,5,"adibou.jpg","pc de bureau",new ArrayList<Joueur>()));
	//cr�ation des joueur
						List<Joueur> les_joueurs = new ArrayList<Joueur>();
						les_joueurs.add(new Joueur(1,"ren�","francais",18));
						les_joueurs.add(new Joueur(2,"pote","francais",28));
						les_joueurs.add(new Joueur(3,"patate","anglais",18));
						les_joueurs.add(new Joueur(4,"michel","allemand",18));
						les_joueurs.add(new Joueur(5,"therempartstrong","finlandais",50));

	//ajout des joueurs qui jouent a 1 ou plusieurs jeux
						les_jeux.get(0).getJoueurs_jeu().add(les_joueurs.get(2));   
						les_jeux.get(0).getJoueurs_jeu().add(les_joueurs.get(4));   
						les_jeux.get(2).getJoueurs_jeu().add(les_joueurs.get(4));
						les_jeux.get(4).getJoueurs_jeu().add(les_joueurs.get(3)); 
						les_jeux.get(3).getJoueurs_jeu().add(les_joueurs.get(1)); 
						Game frame = new Game();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		/**
		 * Create the frame.
		 */
		public Game() {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JComboBox affiche_jeu = new JComboBox();
			affiche_jeu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				
					desc.setText("");
					joueur.setText("");
					prix.setText("");
					date_creation.setText("");
					conf.setText("");
					
					int jeux_Choisi = affiche_jeu.getSelectedIndex();
				//	desc.setText(les_jeux.get(jeux_Choisi).getnom());
					
					desc.setText(les_jeux.get(jeux_Choisi).getDescription());
					prix.setText(""+les_jeux.get(jeux_Choisi).getPrix());
					date_creation.setText(les_jeux.get(jeux_Choisi).getDate_creation());
					conf.setText(les_jeux.get(jeux_Choisi).getConfig_recommandee());
					date_match.setText(les_jeux.get(jeux_Choisi).genere_date_prochain_match());
				
					image.setIcon(new ImageIcon(Game.class.getResource("/gaming/"+les_jeux.get(jeux_Choisi).getImage()+"")));
					
				for (Joueur jo : les_jeux.get(jeux_Choisi).getJoueurs_jeu()) {
					joueur.setText(joueur.getText()+jo.getPseudo()+"\n");
				}	
				}
			});
			affiche_jeu.setBounds(109, 11, 198, 22);
			contentPane.add(affiche_jeu);
			
			
			//zone de texte
			JLabel lblNewLabel = new JLabel("joueur :");
			lblNewLabel.setBounds(10, 133, 46, 23);
			contentPane.add(lblNewLabel);
			
			JLabel lblNewLabel_1 = new JLabel("configuration recommandee : ");
			lblNewLabel_1.setBounds(10, 203, 182, 28);
			contentPane.add(lblNewLabel_1);
			
			JLabel lblConfigurationPrix = new JLabel("prix:");
			lblConfigurationPrix.setBounds(10, 100, 46, 28);
			contentPane.add(lblConfigurationPrix);
			
			JLabel lblDateCreation = new JLabel("date creation :");
			lblDateCreation.setBounds(13, 80, 99, 28);
			contentPane.add(lblDateCreation);
			
			JLabel lblDescription = new JLabel("description :");
			lblDescription.setBounds(13, 54, 86, 28);
			contentPane.add(lblDescription);
			
			// zone o� apparaitront les informations concernant le jeu
			desc = new JLabel("");
			desc.setBounds(98, 54, 125, 28);
			contentPane.add(desc);
			
		    prix  =new JLabel("");
			prix.setBounds(48, 100, 119, 28);
			contentPane.add(prix);
			
		    date_creation = new JLabel("");
		    date_creation.setBounds(98,83, 125,25 );
			contentPane.add(date_creation);
			
		    conf =new JLabel("");
			conf.setBounds(196, 203, 188, 28);
			contentPane.add(conf);
			
			joueur = new JTextArea("");
			joueur.setBackground(SystemColor.control);
			joueur.setBounds(58, 132, 182, 39);
			contentPane.add(joueur);
			
			lblNewLabel_2 = new JLabel("date prochain match :");
			lblNewLabel_2.setBounds(13, 227, 141, 34);
			contentPane.add(lblNewLabel_2);
			
			date_match = new JLabel("");
			date_match.setBounds(151, 227, 131, 34);
			contentPane.add(date_match);
			
			image = new JLabel("");
			//0
			image.setBounds(244, 44, 167, 127);
			contentPane.add(image);
			

				for (Jeu j : les_jeux) {
					affiche_jeu.addItem(j.getNom()); //comboBox donnant les noms des jeux
					
				}
				}
	}

